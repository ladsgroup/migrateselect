#!/usr/bin/env python3

from antlr4 import *
import sys
from PhpParser import PhpParser
import difflib
import re

class MyParseTreeListener(ParseTreeListener):
    def __init__(self, file_name) -> None:
        super().__init__()
        self.file_name = file_name
    def visitTerminal(self, node:TerminalNode):
        pass

    def visitErrorNode(self, node:ErrorNode):
        pass

    def enterEveryRule(self, ctx:ParserRuleContext):
        if isinstance(ctx, PhpParser.MemberAccessContext):
            func_name = ctx.keyedFieldName().getText()
            if func_name.startswith('select'):
                funccallall = ctx.parentCtx.parentCtx.getText()
                if 'newSelectQueryBuilder' in funccallall:
                    return
                var_name = funccallall.split('->select')[0]
                if 'db' in var_name.lower():
                    self.handleWhereFunctionCalls(ctx, var_name, funccallall, func_name)

    def exitEveryRule(self, ctx:ParserRuleContext):
        pass

    def handleWhereFunctionCalls(self, ctx, var_name, funccallall, func_name):
        args = []
        args_tress = []
        for child in ctx.getChildren():
            if isinstance(child, PhpParser.ActualArgumentsContext):
                grandchild = child.getChild(0)
                for greatgrandchild in grandchild.getChildren():
                    if isinstance(greatgrandchild, PhpParser.ActualArgumentContext):
                        args.append(greatgrandchild.getChild(0).getText())
                        args_tress.append(greatgrandchild.getChild(0))
        calls = ['from', 'select', 'where', 'caller', 'options', 'join_conds']
        calls_data = {}
        if func_name == 'select':
            new_func_name = 'fetchResultSet'
        else:
            new_func_name = func_name.replace('select', 'fetch')
        text = var_name + '->newSelectQueryBuilder()\n'
        options = {}
        for i in range(len(args)):
            calls_data[calls[i]] = args[i]
            if calls[i] == 'options':
                for child in args_tress[i].getChildren():
                    if isinstance(child, PhpParser.ArrayCreationContext):
                        for grandchild in child.getChildren():
                            if isinstance(grandchild, PhpParser.ArrayItemListContext):
                                for case in self.handleArrayItems(grandchild):
                                    options[eval(case[0])] = case[1]
        calls_data['options'] = options

        if 'EXPLAIN' in calls_data.get('options', ''):
            text += '->explain()\n'
        if 'select' in calls_data:
            text += '->select( ' + calls_data['select'] + ' )\n'
        if 'FOR UPDATE' in calls_data.get('options', ''):
            text += '->forUpdate()\n'
        if 'LOCK IN SHARE MODE' in calls_data.get('options', ''):
            text += '->lockInShareMode()\n'
        if 'DISTINCT' in calls_data.get('options', ''):
            text += '->distinct()\n'
        if 'from' in calls_data:
            text += '->from( ' + calls_data['from'] + ' )\n'
        if 'where' in calls_data:
            text += '->where( ' + calls_data['where'] + ' )\n'
        if 'GROUP BY' in calls_data.get('options', ''):
            text += '->groupBy( ' + calls_data['options']['GROUP BY'] + ' )\n'
        if 'HAVING' in calls_data.get('options', ''):
            text += '->having( ' + calls_data['options']['HAVING'] + ' )\n'
        if 'ORDER BY' in calls_data.get('options', ''):
            text += '->orderBy( ' + calls_data['options']['ORDER BY'] + ' )\n'
        if 'USE INDEX' in calls_data.get('options', ''):
            text += '->useIndex( ' + calls_data['options']['USE INDEX'] + ' )\n'
        if 'IGNORE INDEX' in calls_data.get('options', ''):
            text += '->ignoreIndex( ' + calls_data['options']['IGNORE INDEX'] + ' )\n'
        if 'LIMIT' in calls_data.get('options', ''):
            text += '->limit( ' + calls_data['options']['LIMIT'] + ' )\n'
        if 'OFFSET' in calls_data.get('options', ''):
            text += '->offset( ' + calls_data['options']['OFFSET'] + ' )\n'
        if 'caller' in calls_data:
            text += '->caller( ' + calls_data['caller'] + ' )'
        text += '->' + new_func_name + '();'
        self.fix_call(funccallall, text, var_name, func_name)

    def handleArrayItems(self, ctx):
        items = []
        for child in ctx.getChildren():
            if not isinstance(child, PhpParser.ArrayItemContext):
                continue
            isAssociative = False
            for grandchild in child.getChildren():
                if isinstance(grandchild, tree.Tree.TerminalNodeImpl) and grandchild.getText() == '=>':
                    isAssociative = True
            if not isAssociative:
                items.append( [child.getChild(0).getText(), True] )
            else:
                items.append( [child.getChild(0).getText(), child.getChild(2).getText()] )
        return items

    def fix_call(self, old_text, new_text, var_name, func_name):
        with open(self.file_name, 'r') as f:
            old_content = f.read()
        new_content = old_content
        var_name = re.sub(r'\((\S+?)\)', r'( \1 )', var_name)
        for search_res in re.findall('(' + re.escape(var_name) + r'\->' + re.escape(func_name) + r'(.+?)\);)', new_content, re.DOTALL):
            search_res = search_res[0]
            cleaned_search_res = self.clean_php(search_res)
            to_check = self.clean_php(old_text) + ';'
            if to_check == cleaned_search_res:
                tabs = new_content.split(search_res)[0].split('\n')[-1].count('\t') + 1
                new_content = new_content.replace(search_res, new_text.replace('\n', '\n' + '\t' * tabs))
            # Part of another statement
            elif cleaned_search_res.startswith(to_check[:-1]):
                extra = cleaned_search_res[len(to_check)-1:]
                tabs = new_content.split(search_res)[0].split('\n')[-1].count('\t') + 1
                new_content = new_content.replace(search_res, new_text[:-1].replace('\n', '\n' + '\t' * tabs) + extra)
        
        if old_content != new_content:
            for line in difflib.unified_diff(old_content.strip().splitlines(), new_content.strip().splitlines(), fromfile='old', tofile='new', lineterm='', n=0):
                print(line)
            okay = input('OK? ')
            if okay.lower() in ['y', 'yes', 'j', 'ja']:
                with open(self.file_name, 'w') as f:
                    f.write(new_content)
    
    def clean_php(self, text):
        new_text = re.sub(r'(\n|^)\s*/\*\*(\s*?.+\n\s*\*|.+\*)*?/', '', text)
        new_text = re.sub(r'(\n|^)\s*//.+', '', new_text)

        new_text = re.sub(r'\n+', '\n', new_text)
        new_text = re.sub(r'([^;}{])\s*\n\s+', r'\1 ', new_text)
        new_text = re.sub(r'\s', '', new_text)
        return new_text