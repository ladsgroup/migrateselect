#!/usr/bin/env python3

from antlr4 import *
import sys
from PhpParser import PhpParser

import re

class MyParseTreeListener(ParseTreeListener):
    def __init__(self, file_name) -> None:
        super().__init__()
        self.file_name = file_name
        self.variables = {}
    def visitTerminal(self, node:TerminalNode):
        pass

    def visitErrorNode(self, node:ErrorNode):
        pass

    def enterEveryRule(self, ctx:ParserRuleContext):
        if isinstance(ctx, PhpParser.ClassStatementContext):
            self.variables = {}
        if isinstance(ctx, PhpParser.AssignmentExpressionContext):
            self.variables[ctx.getChild(0).getText()] = ctx.getChild(2)
        if isinstance(ctx, PhpParser.MemberAccessContext):
            func_name = ctx.keyedFieldName().getText()
            if func_name =='delete':
                funccallall = ctx.parentCtx.parentCtx.getText()
                var_name = ctx.parentCtx.chainOrigin().getText()
                if 'db' in ctx.parentCtx.chainOrigin().getText():
                    self.handleWhereFunctionCalls(ctx, var_name, funccallall)

    def exitEveryRule(self, ctx:ParserRuleContext):
        pass

    def handleWhereFunctionCalls(self, ctx, var_name, funccallall):
        args = []
        for child in ctx.getChildren():
            if isinstance(child, PhpParser.ActualArgumentsContext):
                grandchild = child.getChild(0)
                for greatgrandchild in grandchild.getChildren():
                    if isinstance(greatgrandchild, PhpParser.ActualArgumentContext):
                        args.append(greatgrandchild.getChild(0).getText())
        calls = ['delete', 'where', 'caller']
        calls_data = {}
        text = var_name + '->newDeleteQueryBuilder()\n'
        for i in range(len(args)):
            calls_data[calls[i]] = args[i]
        if 'delete' in calls_data:
            text += '->delete( ' + calls_data['delete'] + ' )\n'
        if 'where' in calls_data:
            text += '->where( ' + calls_data['where'] + ' )\n'
        if 'caller' in calls_data:
            text += '->caller( ' + calls_data['caller'] + ' )'
        text += '->execute();'
        self.fix_call(funccallall, text, var_name)

    def fix_call(self, old_text, new_text, var_name):
        with open(self.file_name, 'r') as f:
            old_content = f.read()
        new_content = old_content
        for search_res in re.findall('(' + re.escape(var_name) + r'\->delete(.+?)\);)', new_content, re.DOTALL):
            search_res = search_res[0]
            cleaned_search_res = self.clean_php(search_res)
            to_check = self.clean_php(old_text) + ';'
            if to_check == cleaned_search_res:
                tabs = new_content.split(search_res)[0].split('\n')[-1].count('\t') + 1
                new_content = new_content.replace(search_res, new_text.replace('\n', '\n' + '\t' * tabs))
        
        if old_content != new_content:
            with open(self.file_name, 'w') as f:
                f.write(new_content)
    
    def clean_php(self, text):
        new_text = re.sub(r'(\n|^)\s*/\*\*(\s*?.+\n\s*\*|.+\*)*?/', '', text)
        new_text = re.sub(r'(\n|^)\s*//.+', '', new_text)

        new_text = re.sub(r'\n+', '\n', new_text)
        new_text = re.sub(r'([^;}{])\s*\n\s+', r'\1 ', new_text)
        new_text = re.sub(r'\s', '', new_text)
        return new_text