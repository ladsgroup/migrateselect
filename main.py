#!/usr/bin/env python3
import re
import glob
import sys
import os
from pathlib import Path

from antlr4 import *
from PhpLexer import PhpLexer
from PhpParser import PhpParser

from parse_trees_select import MyParseTreeListener
import time

def to_search_gen():
    try:
        sys.argv[1]
    except:
        print('Please provide a base path as argument')
        sys.exit()
    if os.path.isdir(sys.argv[1]):
        pathlist = Path(sys.argv[1]).glob('**/*.php')
    else:
        pathlist = sys.argv[1:]
    for php_file in pathlist:
        with open(php_file, 'r') as f:
            code = f.read()
        if re.findall(r'(dbr|\))?->(\w*?select\w*?)\(', code):
            yield str(php_file)

def main():
    for file_name in to_search_gen():
        input_stream = FileStream(file_name, 'utf-8')
        print('--', file_name)
        lexer = PhpLexer(input_stream)
        stream = CommonTokenStream(lexer)
        parser = PhpParser(stream)
        tree = parser.htmlDocument() # TODO: Is this the best root?
        listener = MyParseTreeListener(file_name)
        walker = ParseTreeWalker()
        walker.walk(listener, tree)
        time.sleep(2)


if __name__ == '__main__':
    main()
