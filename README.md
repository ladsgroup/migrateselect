# MigrateSelect

Easier migration of Database::select calls.

## Install

- Install ANTLR4: `sudo apt install antlr4`
- Install ANTLR4 tools: `pip install antlr4-tools`
- Run init.sh: `bash init.sh`

## Running

Run `python main.py /path/to/code/directory`

It will make changes and asks questions before migration. You can accept or reject changes.

## Notes

- This is not perfect and its aim is to take away the bulk of the work not all of it. You still need to make sure it is correct.
- Run phpcbf to clean up linting and spacing issues caused by parsing and replacing. Specially making sure you enable MediaWiki.Arrays.OneSpaceInlineArray.OneSpaceInlineArray fixes
- There are other scripts for migration to InsertQueryBuilder, DeleteQueryBuilder and UpdateQueryBuilder in other_function_calls/ directory
- The migration doesn't really take joins into account, you have to add them manually later.
